<?php
    error_reporting(E_ALL); // เปิด error ทุกอย่าง
    // error_reporting(0); ปิด error ในกรณีที่เราต้องการแสดงเออเล่อของเราเอง

    // เชื่อมต่อ Database
    $conn = new mysqli('localhost','root','','onlinerecruitment');
    $conn->set_charset('utf8'); // ให้รองรับภาษาไทย

    if ($conn->connect_errno){
        echo "Connect Error: ".$conn->connect_error;
        exit();
    }

?>