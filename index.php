<?php
    require_once('php/connect.php');

    // echo $_GET['tag'];

    // if แบบ เต็ม
    // if(isset( $_GET['tag'])){
    //     $tag = $_GET['tag'];
    // } else {
    //     $tag = 'all';
    // }

    $sql = "SELECT * FROM `articles` WHERE `status` = 'true' ORDER BY RAND() LIMIT 6";
    $result = $conn->query($sql);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--โทรศัพท์ซูมไม่ได้-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carpet Maker (Thailand)</title>

<!-- Search Engine -->
    <meta name="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่">
    <meta name="keywords" content="Carpet Maker (Thailand), Carpet Maker, หางาน">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="Carpet Maker (Thailand)">

<!-- Schema.org for Google -->
    <meta itemprop="name" content="Carpet Maker (Thailand)">
    <meta itemprop="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta itemprop="image" content="#image">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Carpet Maker (Thailand)">
    <meta name="og:description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta name="og:image" content="#image">
    <meta name="og:url" content="#linkwebsite">
    <meta name="og:site_name" content="#linkwebsite">
    <meta name="og:type" content="website">

<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">
    
    
</head>
<body>

<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>
    
<!-- Section Carousel -->
    <div class="mt-5">
        <header class="jarallax" style="background-image: url(assets/images/header.jpg);">
            <div class="page-image">
                <h1 class="display-3 font-weight-bold">Carpet Maker (Thailand)</h1>
            </div>
        </header>
    </div>

<!-- Section Hope -->

    

<!-- Section Blog -->

    <section class="container mt-2">
        <h1 class="border-short-bottom text-center">ประชาสัมพันธ์</h1>
        <div class="row">

        <?php while($row = $result->fetch_assoc()){  ?>
        <section class="col-12 col-sm-6 col-md-4 p-2">
            <div class="card h-100">
                <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="warpper-card-img">
                    <img class="card-img-top" src="<?php echo $base_path_blog.$row['image'] ?>" alt="Card image cap">
                </a>
                <div class="card-body">
                    <h5 class="card-title"><?php echo $row['subject'] ?></h5>
                    <p class="card-text"><?php echo $row['subtitle'] ?></p>
                </div>
                <div class="p-3">
                    <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                </div>
            </div>
        </section>
        <?php } ?>
        </div>
    </section>
    
    
   
    

<!-- Section About & Footer  -->

    <?php include_once('includes/footer.php') ?>

<!-- Section On to top -->
    <div class="to-top">
        <i class="fa fa-angle-up"></i>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>
</body>
</html>