<?php
    require_once('php/connect.php');
    $sql = "SELECT * FROM articles WHERE id = '".$_GET['id']."' AND `status` = 'true' ";
    $result = $conn->query($sql);

    if ($result->num_rows) {
        $row = $result->fetch_assoc();
    } else {
        header('location: blog.php');
    }

    // OWL

    $sql_RAND = "SELECT * FROM `articles` WHERE `status` = 'true' ORDER BY RAND() LIMIT 6";
    $result_RAND = $conn->query($sql_RAND);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--โทรศัพท์ซูมไม่ได้-->
    <meta property="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $row['subject']; ?></title>
    
<!-- Search Engine -->
    <meta property="description" content="<?php echo $row['subtitle']; ?>">
    <meta property="keywords" content="#keyword">
    <meta property="robots" content="index, nofollow">
    <meta property="web_author" content="#title">

<!-- Schema.org for Google -->
    <meta property="name" content="#title">
    <meta property="description" content="<?php echo $row['subtitle']; ?>">
    <meta property="image" content="#picture">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta property="og:title" content="#title">
    <meta property="og:description" content="<?php echo $row['subtitle']; ?>">
    <meta property="og:image" content="[#picturelink]">
    <meta property="og:url" content="#url">
    <meta property="og:site_name" content="#url">
    <meta property="og:type" content="website">

<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">
    
    
</head>
<body>

<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

<!-- Section Page-Title -->

    <header class="jarallax mt-5" style="background-image: url(<?php echo $base_path_blog.$row['image']; ?>);">
        <div class="page-image">
            <h1 class="display-4 font-weight-bold"><?php echo $row['subject']; ?>  </h1>
            <p class="lead"><?php echo $row['subtitle']; ?></p>
        </div>
    </header>

<!-- Section Blog -->
    <section class="container blog-content">
        <div class="row">
            <div class="col-12">
                <?php echo $row['detail']; ?>    
            </div>
            <div class="col-12 text-right">
                <hr>
                <div class="pw-server-widget" data-id="wid-aplrceff"></div>
                <p class=" text-muted"> <?php echo date_format(new DateTime($row['updated_at']),"j F Y"); ?> </p>
            </div>
            
            <!-- OWL-CAROUSEL -->
            <div class="col-12">
                <div class="owl-carousel owl-theme">
                    <?php while ($row_RAND = $result_RAND->fetch_assoc()) { ?>
                    <section class="col-12 p-2">
                        <div class="card h-100">
                            <a href="blog-detail.php?id=<?php echo $row_RAND['id'] ?>" class="warpper-card-img">
                                <img class="card-img-top" src="<?php echo $base_path_blog.$row_RAND['image'] ?>" alt="Card image cap">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $row_RAND['subject'] ?></h5>
                                <p class="card-text"><?php echo $row_RAND['subtitle'] ?></p>
                            </div>
                            <div class="p-3">
                                <a href="blog-detail.php?id=<?php echo $row_RAND['id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                            </div>
                        </div>
                    </section>
                    <?php } ?>
                </div>
            </div>

            <!-- facebook div -->
            <div class="col-12">
                <div class="fb-comments" 
                data-width="100%" 
                data-href="http://localhost/blog/blog-detail.php?id_bankzveryhansome=<?php echo $row['id']; ?>" 
                data-numposts="5"></div>
                <div id="fb-root"></div>
            </div>
        </div>
    </section>

<!-- Section About & Footer  -->

    <?php include_once('includes/footer.php') ?>

<!-- Section On to top -->
    <div class="to-top">
        <i class="fa fa-angle-up"></i>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>

    <!-- OWL CAROUSEL -->
    <script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:2,
                },
                1000:{
                    items:3,
                }
            }
        });
    });

  //              DO NOT IMPLEMENT                //
  //       this code through the following        //
  //                                              //
  //   Floodlight Pixel Manager                   //
  //   DCM Pixel Manager                          //
  //   Any system that places code in an iframe   //
    (function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
          + '.po.st/static/v4/post-widget.js#publisherKey=68je62lf37960jkpap5t';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
     })();

    </script>
</body>
</html>