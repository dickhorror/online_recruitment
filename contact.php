<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--โทรศัพท์ซูมไม่ได้-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact</title>

<!-- Search Engine -->
<meta name="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่">
    <meta name="keywords" content="Carpet Maker (Thailand), Carpet Maker, หางาน">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="Carpet Maker (Thailand)">

<!-- Schema.org for Google -->
    <meta itemprop="name" content="Carpet Maker (Thailand)">
    <meta itemprop="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta itemprop="image" content="#image">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Carpet Maker (Thailand)">
    <meta name="og:description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta name="og:image" content="#image">
    <meta name="og:url" content="#linkwebsite">
    <meta name="og:site_name" content="#linkwebsite">
    <meta name="og:type" content="website">


<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">
</head>
<body>

<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

<!-- Section Page-Title -->

    <header class="jarallax" style="background-image: url(https://images.unsplash.com/photo-1527067669193-6a36380de229?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=075d07fb2cee915ef07dee0d73cbdd54&auto=format&fit=crop&w=1236&q=80);">
        <div class="page-image">
            <h1 class="display-4 font-weight-bold">ข้อมูลการติดต่อ</h1>
            <p class="lead">โปรดกรอกแบบฟอร์มด้านล่างและเราจะติดต่อกลับโดยเร็วที่สุด</p>
        </div>
    </header>

<!-- Section Blog -->
    <section class="container py-5">
        <div class="row text-center">
            <div class="col-sm-4 mb-2">
                <div class="card h-100">
                    <div class="card-body">
                        <i class="fa fa-address-card py-2 fa-4x text-info"></i>
                        <h4 class="card-title">ที่อยู่</h4>
                        <p class="card-text">บริษัท คาร์เปท เมกเกอร์ (ประเทศไทย) จำกัด<br>194 หมู่ 1 ถนนแจ้งสนิท ตำบลเมืองเพีย อำเภอบ้านไผ่ จังหวัดขอนแก่น 40110</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card h-100">
                    <div class="card-body">
                        <i class="fa fa-phone-square py-2 fa-4x text-info"></i>
                        <h4 class="card-title">เบอร์โทรศัพท์</h4>
                        <p class="card-text">043-286-7346</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card h-100">
                    <div class="card-body">
                        <i class="fa fa-envelope-square py-2 fa-4x text-info"></i>
                        <h4 class="card-title">อีเมลล์</h4>
                        <p class="card-text">example@email.com</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">แบบฟอร์มติดต่อเรา</h5>
                        <form method="post" action="php/contact.php">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="name">ชื่อ-นามสกุล</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ชื่อของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Phone">เบอร์โทรศัพท์</label>
                                    <input type="text" id="Phone" name="phone" class="form-control" required placeholder="เบอร์โทรศัพท์ของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">อีเมลล์</label>
                                    <input type="text" id="email" name="email" class="form-control" required placeholder="example@email.com">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message">ข้อความของคุณ</label>
                                <textarea name="" id="message" name="message" class="form-control" required placeholder="เขียนข้อความของคุณที่นี่" rows="5"></textarea>
                            </div>
                            
                            <div id="recaptcha-wrapper" class="text-center mb-2">
                                <div class="g-recaptcha d-inline-block" data-sitekey="6Lfjr2oUAAAAAFFpVcPb3pSUspQThs0qmLMJJbtn"></div>
                            </div>

                            <button type="submit" class="btn btn-primary d-block mx-auto">ส่งข้อความ</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-1">
                <h3>แผนที่</h3>
                <div id="map"></div>
            </div>
        </div>
    </div>


<!-- Section About & Footer  -->

    <?php include_once('includes/footer.php') ?>

<!-- Section On to top -->
    <div class="to-top">
        <i class="fa fa-angle-up"></i>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>

    <!-- Script reCAPTCHA -->
    <script>
        $(function(){
            // global variables
            captchaResized = false;
            captchaWidth = 304;
            captchaHeight = 78;
            captchaWrapper = $('#recaptcha-wrapper');
            captchaElements = $('#rc-imageselect, .g-recaptcha');

            $(window).on('resize', function() {
                resizeCaptcha();
            });

            resizeCaptcha();
        });

        function resizeCaptcha() {
            if (captchaWrapper.width() >= captchaWidth) {
                if (captchaResized) {
                    captchaElements.css('transform', '').css('-webkit-transform', '').css('-ms-transform', '').css('-o-transform', '').css('transform-origin', '').css('-webkit-transform-origin', '').css('-ms-transform-origin', '').css('-o-transform-origin', '');
                    captchaWrapper.height(captchaHeight);
                    captchaResized = false;
                }
            } else {
                var scale = (1 - (captchaWidth - captchaWrapper.width()) * (0.05/15));
                captchaElements.css('transform', 'scale('+scale+')').css('-webkit-transform', 'scale('+scale+')').css('-ms-transform', 'scale('+scale+')').css('-o-transform', 'scale('+scale+')').css('transform-origin', '0 0').css('-webkit-transform-origin', '0 0').css('-ms-transform-origin', '0 0').css('-o-transform-origin', '0 0');
                captchaWrapper.height(captchaHeight * scale);
                if (captchaResized == false) captchaResized = true;
            }
        }
        // resizeCaptcha();
    </script>
</body>
</html>