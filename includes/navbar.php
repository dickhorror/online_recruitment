<?php $file_name = basename($_SERVER['SCRIPT_FILENAME'],".php"); ?>
<nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-dark bg-purple">
    <div class="container">
        <div class="row">
            <div class="col-12 d-block">
                <a class="navbar-brand" href="index.php">
                    <img src="assets/images/Carpet_logo.svg" width="80%" height="80%" class="d-inline-block align-top" alt="">
                </a>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey" aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarKey">
            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item <?php echo $file_name == 'index' ? 'active': '' ?>">
                    <a class="nav-link" href="index.php">หน้าแรก <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <?php echo $file_name == 'about' ? 'active': '' ?>">
                    <a class="nav-link" href="about.php">เกี่ยวกับเรา</a>
                </li>
                <li class="nav-item <?php echo $file_name == 'blog' || $file_name == 'blog-detail' ? 'active': '' ?>">
                    <a class="nav-link" href="blog.php">หางาน</a>
                </li>
                <li class="nav-item <?php echo $file_name == 'formjob' ? 'active': '' ?>">
                    <a class="nav-link" href="formjob.php">กรอกฟอร์มสมัครงาน</a>
                </li>
                <li class="nav-item <?php echo $file_name == 'contact' ? 'active': '' ?>">
                    <a class="nav-link" href="contact.php">ติดต่อเรา</a>
                </li>
                <li class="nav-item <?php echo $file_name == 'login' ? 'active': '' ?>">
                    <a class="nav-link" href="login/login.php">Admin Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>