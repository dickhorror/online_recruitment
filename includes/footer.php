<?php $file_name = basename($_SERVER['SCRIPT_FILENAME'],".php"); ?>

<footer class="semi-footer mt-4 p-5 text-center text-md-left bg-purple">
    <div class="row">
        <div class="col-md-4">
            <a class="navbar-brand" href="index.php">
                <img src="assets/images/Carpet_logo.svg" width="100%" height="100%" class="d-inline-block align-top" alt="">
            </a>
        </div>
        <div class="col-md-2">
            <p>FOLLOW US<br>#thecarpetmaker</p>
        </div>
        <div class="col-md-6 fix-decoration">
            <a href="https://www.facebook.com/TheCarpetMaker/" target="_blank">
                <i class="fa fa-facebook fa-2x"></i>
            </a>
            <a href="https://www.instagram.com/thecarpetmaker/" target="_blank">
                <i class="fa fa-instagram fa-2x"></i>
            </a>
            <a href="https://www.pinterest.com/carpetmaker/" target="_blank">
                <i class="fa fa-pinterest fa-2x"></i>
            </a>
            <p>
                Term & Conditions
                Copyright © 2018-2019 the carpet maker. All Right Reserves.
                Designed By Suriya
            </p>
        </div>
    </div>
</footer>