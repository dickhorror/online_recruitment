// facebook comment plugin

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.1&appId=110057799331998&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


// Initialize and add the map

function initMap() {
    var uluru = {lat: 16.0811293, lng: 102.6462356};
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 15, center: uluru});
    var marker = new google.maps.Marker({position: uluru, map: map});
  }

// navbar animation

$(window).scroll(function(e){
    var scrollTop = $(this).scrollTop();
    if (scrollTop > 1){
        $('#navbar').css('padding','0 20px')
    }
    else{
        $('#navbar').css('padding','20px')
    }
})

$('.jarallax').jarallax();

// On To Top

$('.to-top').click(function (){
    $('html, body').animate({scrollTop: '0px'}, 800);
})