<?php 
    require_once('php/connect.php');
    $tag = isset($_GET['tag']) ? $_GET['tag'] : 'all';
    $sql = "SELECT * FROM `articles` WHERE `tag` LIKE '%".$tag."%' AND `status` = 'true'";
    $result = $conn->query($sql);
    if (!$result) {
        header('Location: blog.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Job Detail</title>
    
<!-- COMMON TAGS -->
    <meta charset="utf-8">

<!--โทรศัพท์ซูมไม่ได้-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About</title>

<!-- Search Engine -->
    <meta name="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่">
    <meta name="keywords" content="Carpet Maker (Thailand), Carpet Maker, หางาน">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="Carpet Maker (Thailand)">

<!-- Schema.org for Google -->
    <meta itemprop="name" content="Carpet Maker (Thailand)">
    <meta itemprop="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta itemprop="image" content="#image">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Carpet Maker (Thailand)">
    <meta name="og:description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta name="og:image" content="#image">
    <meta name="og:url" content="#linkwebsite">
    <meta name="og:site_name" content="#linkwebsite">
    <meta name="og:type" content="website">


<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">

    
</head>
<body>
    
<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

<!-- Section Page-title -->
    <header class="jarallax mt-5" data-speed="0.5" style="background-image: url(assets/images/job.jpg);">
        <div class="page-image">
            <h1 class="display-4 font-weight-bold">หางานเลือกงานของคุณ</h1>
            <p class="lead">"เลือกประเภทงานที่คุณสนใจ"</p>
        </div>
    </header>

<!-- Section Blog -->
    <section class="container py-5">
        <div class="row pb-4">
            <div class="col-12 text-center">
                <div class="btn-group-custom">
                    <a href="blog.php?tag=all">
                        <button class="btn btn-primary <?php echo $tag == 'all' ? 'active': '' ?> ">ทั้งหมด</button>
                    </a>
                    <a href="blog.php?tag=graphic">
                        <button class="btn btn-primary <?php echo $tag == 'Graphic' ? 'active': '' ?>">กราฟฟิค</button>
                    </a>
                    <a href="blog.php?tag=Sale">
                        <button class="btn btn-primary <?php echo $tag == 'Sale department' ? 'active': '' ?>">ขาย</button>
                    </a>
                    <a href="blog.php?tag=computer">
                        <button class="btn btn-primary <?php echo $tag == 'Computer' ? 'active': '' ?>">คอมพิวเตอร์</button>
                    </a>
                    <a href="blog.php?tag=accounting">
                        <button class="btn btn-primary <?php echo $tag == 'Accounting' ? 'active': '' ?>">บัญชี</button>
                    </a>
                    <a href="blog.php?tag=production-department">
                        <button class="btn btn-primary <?php echo $tag == 'Production department' ? 'active': '' ?>">ฝ่ายผลิต</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <?php 
            if ($result->num_rows){
                while($row = $result->fetch_assoc()) { 
            ?>
            <section class="col-12 col-sm-6 col-md-4 p-2">
                <div class="card h-100">
                    <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="warpper-card-img">
                        <img class="card-img-top" src="<?php echo $base_path_blog.$row['image'] ?>" alt="Coding">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['subject'] ?></h5>
                        <p class="card-text"><?php echo $row['subtitle'] ?></p>
                    </div>
                    <div class="p-3">
                        <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <?php 
                }
            } else {
            ?>
            <section class="col-12">
                <p class="text-center">ไม่มีข้อมูล</p>
            </section>  
            <?php 
            } 
            ?>      

        </div>
    </section>

<!-- Section Footer -->
    <?php include_once('includes/footer.php') ?>

    <div class="to-top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
        

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>

</body>
</html>