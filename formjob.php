<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--โทรศัพท์ซูมไม่ได้-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact</title>

<!-- Search Engine -->
<meta name="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่">
    <meta name="keywords" content="Carpet Maker (Thailand), Carpet Maker, หางาน">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="Carpet Maker (Thailand)">

<!-- Schema.org for Google -->
    <meta itemprop="name" content="Carpet Maker (Thailand)">
    <meta itemprop="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta itemprop="image" content="#image">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Carpet Maker (Thailand)">
    <meta name="og:description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta name="og:image" content="#image">
    <meta name="og:url" content="#linkwebsite">
    <meta name="og:site_name" content="#linkwebsite">
    <meta name="og:type" content="website">


<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">
</head>
<body>

<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

<!-- Section Page-Title -->

    <header class="jarallax" style="background-image: url(assets/images/formjob.jpg);">
        <div class="page-image">
            <h1 class="display-4 font-weight-bold">ส่งแบบฟอร์มสมัครงาน</h1>
            <p class="lead">โปรดกรอกแบบฟอร์มด้านล่างและเราจะติดต่อกลับโดยเร็วที่สุด</p>
        </div>
    </header>

<!-- Section Blog -->
    <section class="container py-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">แบบฟอร์มสมัครงาน</h5>
                        <p>* สำคัญต้องกรอก</p>
                        <form method="post" action="php/contact.php">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="name">ชื่อ-นามสกุล*</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ชื่อของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Phone">เบอร์โทรศัพท์*</label>
                                    <input type="text" id="Phone" name="phone" class="form-control" required placeholder="เบอร์โทรศัพท์ของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">อีเมลล์*</label>
                                    <input type="text" id="email" name="email" class="form-control" required placeholder="example@email.com">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="nationality">สัญชาติ*</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ระบุสัญชาติของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="status">สถานภาพ*</label>
                                    <select class="form-control">
                                        <option>โสด</option>
                                        <option>สมรส</option>
                                        <option>หย่า</option>
                                        <option>หม้าย</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="sex">เพศ*</label>
                                    <select class="form-control">
                                        <option>ชาย</option>
                                        <option>หญิง</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Religion">ศาสนา*</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ระบุศาสนาของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Religion">วัน/เดือน/ปี เกิด*</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ระบุ วัน/เดือน/ปี เกิดของคุณ">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Position">ตำแหน่งที่ต้องการสมัคร*</label>
                                    <input type="text" id="name" name="name" class="form-control" required placeholder="ระบุตำแหน่งที่คุณต้องการสมัคร">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">ส่ง Resume ของคุณที่นี่</label>
                                        <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="message">ข้อมูลการศึกษา*</label>
                                <textarea name="" id="message" name="message" class="form-control" required placeholder="ระบุข้อมูลการศึกษาของคุณ" rows="5"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="message">ข้อมูลอื่นๆ หรือ ข้อความที่คุณต้องการส่งถึงเรา</label>
                                <textarea name="" id="message" name="message" class="form-control" placeholder="ระบุ ข้อมูลอื่นๆ หรือ ข้อความที่คุณต้องการส่งถึงเรา" rows="5"></textarea>
                            </div>
                            
                            <div id="recaptcha-wrapper" class="text-center mb-2">
                                <div class="g-recaptcha d-inline-block" data-sitekey="6Lfjr2oUAAAAAFFpVcPb3pSUspQThs0qmLMJJbtn"></div>
                            </div>

                            <button type="submit" class="btn btn-primary d-block mx-auto">ส่งข้อความ</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

<!-- Section About & Footer  -->

    <?php include_once('includes/footer.php') ?>

<!-- Section On to top -->
    <div class="to-top">
        <i class="fa fa-angle-up"></i>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>

    <!-- Script reCAPTCHA -->
    <script>
        $(function(){
            // global variables
            captchaResized = false;
            captchaWidth = 304;
            captchaHeight = 78;
            captchaWrapper = $('#recaptcha-wrapper');
            captchaElements = $('#rc-imageselect, .g-recaptcha');

            $(window).on('resize', function() {
                resizeCaptcha();
            });

            resizeCaptcha();
        });

        function resizeCaptcha() {
            if (captchaWrapper.width() >= captchaWidth) {
                if (captchaResized) {
                    captchaElements.css('transform', '').css('-webkit-transform', '').css('-ms-transform', '').css('-o-transform', '').css('transform-origin', '').css('-webkit-transform-origin', '').css('-ms-transform-origin', '').css('-o-transform-origin', '');
                    captchaWrapper.height(captchaHeight);
                    captchaResized = false;
                }
            } else {
                var scale = (1 - (captchaWidth - captchaWrapper.width()) * (0.05/15));
                captchaElements.css('transform', 'scale('+scale+')').css('-webkit-transform', 'scale('+scale+')').css('-ms-transform', 'scale('+scale+')').css('-o-transform', 'scale('+scale+')').css('transform-origin', '0 0').css('-webkit-transform-origin', '0 0').css('-ms-transform-origin', '0 0').css('-o-transform-origin', '0 0');
                captchaWrapper.height(captchaHeight * scale);
                if (captchaResized == false) captchaResized = true;
            }
        }
        // resizeCaptcha();
    </script>
</body>
</html>