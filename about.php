<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--โทรศัพท์ซูมไม่ได้-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About</title>
    
<!-- Search Engine -->
<meta name="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่">
    <meta name="keywords" content="Carpet Maker (Thailand), Carpet Maker, หางาน">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="Carpet Maker (Thailand)">

<!-- Schema.org for Google -->
    <meta itemprop="name" content="Carpet Maker (Thailand)">
    <meta itemprop="description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta itemprop="image" content="#image">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Carpet Maker (Thailand)">
    <meta name="og:description" content="หางาน Carpet Maker สมัครงานง่าย ใบสมัครถึง HR ทันที งานมั่นคง เงินเดือนดี มีอยู่จริง ที่นี่ ">
    <meta name="og:image" content="#image">
    <meta name="og:url" content="#linkwebsite">
    <meta name="og:site_name" content="#linkwebsite">
    <meta name="og:type" content="website">


<!-- Favicons -->
    

<!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylemain.css">
    
</head>
<body>

<!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

<!-- Section Page-Title -->
 

    <header class="jarallax" style="background-image: url(https://images.unsplash.com/photo-1508834948694-2011e9c69c17?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=3055c8abf89ac4eda4f946703fc832ab&auto=format&fit=crop&w=1387&q=80);">
        <div class="page-image">
            <h1 class="display-4 font-weight-bold">เกี่ยวกับเรา</h1>
            <p class="lead">About Company</p>
        </div>
    </header>

<!-- Section Todo -->

    <section class="container py-5">
        <div class="row">
            <div class="col-12 py-3 p-lg-0">
                <p>Carpet Maker (Thailand) Co., Ltd. is gaining a reputation as the most reliable high-end 
                    custom made carpet manufacturer. With more than 26 years' experience, we know how to cover a floor.</p>
                <br>
                <p>Established in 1986 by our Managing Director, Mr. Soonthorn Kraitrakul, Carpet Maker (Thailand) 
                    arose from his innate fascination with the intricacy and exquisite craftsmanship of villagers 
                    in Northeastern Thailand. He selected a 22-acre site in Ban Phai, Khon Kaen province 
                    to establish his first hand-tufted carpet factory with an initial staff of only 7 workers.</p>
            </div>
        </div>
    </section>

<!-- Section Timeline -->

    <section class="position-relative  py-5 jarallax" style="background-image: url(assets/images/timeline.jpg);">
        <div class="container">
            <div class="row ">
                <div class="col-12 text-center picturepx">
                    <img src="assets/images/logo1.png" class="img-fluid" alt="">
                    <h1 class="text-white display-4 font-weight-bold">Time line About Us</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge">
                            <p>xxxxxxxxx</p>
                        </div>
                        <div class="timeline-card">
                            <h5>xxxxxxxxx</h5>
                            <p class="text-muted">xxxxxxxxx</p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p>xxxxxxxxx</p>
                        </div>
                        <div class="timeline-card">
                            <h5>xxxxxxxxx</h5>
                            <p class="text-muted">xxxxxxxxx</p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p>xxxxxxxxx</p>
                        </div>
                        <div class="timeline-card">
                            <h5>xxxxxxxxx</h5>
                            <p class="text-muted">xxxxxxxxx</p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p>xxxxxxxxx</p>
                        </div>
                        <div class="timeline-card">
                            <h5>xxxxxxxxx</h5>
                            <p class="text-muted">xxxxxxxxx</p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p>xxxxxxxxx</p>
                        </div>
                        <div class="timeline-card">
                            <h5>xxxxxxxxx</h5>
                            <p class="text-muted">xxxxxxxxx<br>xxxxxxxxx</p>
                        </div>
                    </li>
                    <li class="timeline-arrow">
                        <i class="fa fa-chevron-down"></i>
                    </li>
                </ul>
            </div>
        </div>
    </section>

<!-- Section About & Footer  -->

    <?php include_once('includes/footer.php') ?>

<!-- Section On to top -->
    <div class="to-top">
        <i class="fa fa-angle-up"></i>
    </div>  

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/jsmain.js"></script>

</body>
</html>